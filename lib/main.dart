import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///NOTES FOR PROFESSOR:
///1) PRESS EACH ICON ONCE, IT DOESN'T UPDATE PHOTO UNLESS YOU REFRESH THE PAGE BY CLICKING A DIFFERENT PAGE
///2)

void main() {runApp(MyApp()); }

double width = 300;
double height = 150;
bool h1 = false;
bool h2 = false;
bool h3 = false;
bool h4 = false;
bool h5 = false;
bool h6 = false;
bool h7 = false;
bool h8 = false;
bool c1 = false;
bool c2 = false;
bool c3 = false;
bool c4 = false;
bool c5 = false;
bool c6 = false;
bool c7 = false;
bool c8 = false;
bool a1 = false;
bool a2 = false;
bool a3 = false;
bool a4 = false;
bool a5 = false;
bool a6 = false;
bool a7 = false;
bool a8 = false;


void saveHome(String homeName) {
  if(homeName == 'h1')
    h1 = !h1;

  if(homeName == 'h2')
    h2 = !h2;

  if(homeName == 'h3')
    h3 = !h3;

  if(homeName == 'h4')
    h4 = !h4;

  if(homeName == 'h5')
    h5 = !h5;

  if(homeName == 'h6')
    h6 = !h6;

  if(homeName == 'h7')
    h7 = !h7;

  if(homeName == 'h8')
    h8 = !h8;
}

void saveCondo(String condoName) {
  if(condoName == 'c1')
    c1 = !c1;

  if(condoName == 'c2')
    c2 = !c2;

  if(condoName == 'c3')
    c3 = !c3;

  if(condoName == 'c4')
    c4 = !c4;

  if(condoName == 'c5')
    c5 = !c5;

  if(condoName == 'c6')
    c6 = !c6;

  if(condoName == 'c7')
    c7 = !c7;

  if(condoName == 'c8')
    c8 = !c8;
}

void saveApartment(String apartmentName) {
  if(apartmentName == 'a1')
    a1 = !a1;

  if(apartmentName == 'a2')
    a2 = !a2;

  if(apartmentName == 'a3')
    a3 = !a3;

  if(apartmentName == 'a4')
    a4 = !a4;

  if(apartmentName == 'a5')
    a5 = !a5;

  if(apartmentName == 'a6')
    a6 = !a6;

  if(apartmentName == 'a7')
    a7 = !a7;

  if(apartmentName == 'a8')
    a8 = !a8;
}

bool sh1 = false;
bool sh2 = false;
bool sh3 = false;
bool sh4 = false;
bool sh5 = false;
bool sh6 = false;
bool sh7 = false;
bool sh8 = false;
bool sc1 = false;
bool sc2 = false;
bool sc3 = false;
bool sc4 = false;
bool sc5 = false;
bool sc6 = false;
bool sc7 = false;
bool sc8 = false;
bool sa1 = false;
bool sa2 = false;
bool sa3 = false;
bool sa4 = false;
bool sa5 = false;
bool sa6 = false;
bool sa7 = false;
bool sa8 = false;

void scheduleAppointment(String type) {
  if(type == 'h1')
    sh1 = !sh1;

  if(type == 'h2')
    sh2 = !sh2;

  if(type == 'h3')
    sh3 = !sh3;

  if(type == 'h4')
    sh4 = !sh4;

  if(type == 'h5')
    sh5 = !sh5;

  if(type == 'h6')
    sh6 = !sh6;

  if(type == 'h7')
    sh7 = !sh7;

  if(type == 'h8')
    sh8 = !sh8;

  ///---------------------------------------------------------------------------

  if(type == 'c1')
    sc1 = !sc1;

  if(type == 'c2')
    sc2 = !sc2;

  if(type == 'c3')
    sc3 = !sc3;

  if(type == 'c4')
    sc4 = !sc4;

  if(type == 'c5')
    sc5 = !sc5;

  if(type == 'c6')
    sc6 = !sc6;

  if(type == 'c7')
    sc7 = !sc7;

  if(type == 'c8')
    sc8 = !sc8;

  ///---------------------------------------------------------------------------

  if(type == 'a1')
    sa1 = !a1;

  if(type == 'a2')
    sa2 = !a2;

  if(type == 'a3')
    sa3 = !a3;

  if(type == 'a4')
    sa4 = !a4;

  if(type == 'a5')
    sa5 = !a5;

  if(type == 'a6')
    sa6 = !a6;

  if(type == 'a7')
    sa7 = !a7;

  if(type == 'a8')
    sa8 = !a8;
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 5',
      home: HomePage(),
    );
  }
}

///Navigation Pages
///-----------------------------------------------------------------------------
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController = PageController();
  List<Widget> _screens = [ ExplorePage(), Library() ];
  int _selectedIndex = 0;

  void _onPageChanged(int index) { setState(() {_selectedIndex = index;});}
  void _onitemTapped(int selectedIndex) {_pageController.jumpToPage(selectedIndex);}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),

      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.pink[400],
        onTap: _onitemTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home, color: _selectedIndex == 0 ? Colors.grey[200] : Colors.grey[400],),
            title: Text('Explore', style: TextStyle(color: _selectedIndex == 0 ? Colors.grey[200] : Colors.grey[400])),
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.book, color: _selectedIndex == 1 ? Colors.grey[200] : Colors.grey[400]),
            title: Text('Library', style: TextStyle(color: _selectedIndex == 1 ? Colors.grey[200] : Colors.grey[400])),
          ),
        ],
      ),
    );
  }
}

///Explore Page
///-----------------------------------------------------------------------------
class ExplorePage extends StatefulWidget {
  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text('Explore'),
            backgroundColor: Colors.pink[400],
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  text: 'Homes',
                  icon: Icon(Icons.home),
                ),
                Tab(
                  text: 'Condos',
                  icon: Icon(Icons.weekend),
                ),
                Tab(
                  text: 'Apartments',
                  icon: Icon(Icons.people),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[ Homes(), Condos(), Apartments()],
          ),
        )
    );
  }
}

///Homes TAB
///-----------------------------------------------------------------------------
class Homes extends StatefulWidget {
  @override
  _HomesState createState() => _HomesState();
}

class _HomesState extends State<Homes> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h1.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh1 ? Text('Removed Appointment for Home 1!') : Text('Scheduled Appointment for Home 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h1 ? Text('Removed Home 1!') : Text('Saved Home 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h2.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Home 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sh2 ? Text('Removed Appointment for Home 2!') : Text('Scheduled Appointment for Home 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('h2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sh2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: h2 ? Text('Removed Home 2!') : Text('Saved Home 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveHome('h2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: h2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h3.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh3 ? Text('Removed Appointment for Home 3!') : Text('Scheduled Appointment for Home 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h3 ? Text('Removed Home 3!') : Text('Saved Home 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h4.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Home 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sh4 ? Text('Removed Appointment for Home 4!') : Text('Scheduled Appointment for Home 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('h4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sh4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: h4 ? Text('Removed Home 4!') : Text('Saved Home 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveHome('h4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: h4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h5.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh5 ? Text('Removed Appointment for Home 5!') : Text('Scheduled Appointment for Home 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h5 ? Text('Removed Home 5!') : Text('Saved Home 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h6.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Home 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sh6 ? Text('Removed Appointment for Home 6!') : Text('Scheduled Appointment for Home 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('h6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sh6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: h6 ? Text('Removed Home 6!') : Text('Saved Home 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveHome('h6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: h6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h7.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh7 ? Text('Removed Appointment for Home 7!') : Text('Scheduled Appointment for Home 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h7 ? Text('Removed Home 7!') : Text('Saved Home 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/h8.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Home 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sh8 ? Text('Removed Appointment for Home 8!') : Text('Scheduled Appointment for Home 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('h8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sh8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: h8 ? Text('Removed Home 8!') : Text('Saved Home 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveHome('h8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: h8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Condos TAB
///-----------------------------------------------------------------------------
class Condos extends StatefulWidget {
  @override
  _CondosState createState() => _CondosState();
}

class _CondosState extends State<Condos> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c1.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc1 ? Text('Removed Appointment for Condo 1!') : Text('Scheduled Appointment for Condo 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c1 ? Text('Removed Condo 1!') : Text('Saved Condo 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c2.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Condo 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sc2 ? Text('Removed Appointment for Condo 2!') : Text('Scheduled Appointment for Condo 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('c2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sc2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: c2 ? Text('Removed Condo 2!') : Text('Saved Condo 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveCondo('c2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: c2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c3.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc3 ? Text('Removed Appointment for Condo 3!') : Text('Scheduled Appointment for Condo 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c3 ? Text('Removed Condo 3!') : Text('Saved Condo 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c4.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Condo 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sc4 ? Text('Removed Appointment for Condo 4!') : Text('Scheduled Appointment for Condo 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('c4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sc4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: c4 ? Text('Removed Condo 4!') : Text('Saved Condo 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveCondo('c4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: c4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c5.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc5 ? Text('Removed Appointment for Condo 5!') : Text('Scheduled Appointment for Condo 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c5 ? Text('Removed Condo 5!') : Text('Saved Condo 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c6.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Condo 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sc6 ? Text('Removed Appointment for Condo 6!') : Text('Scheduled Appointment for Condo 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('c6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sc6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: c6 ? Text('Removed Condo 6!') : Text('Saved Condo 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveCondo('c6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: c6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c7.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc7 ? Text('Removed Appointment for Condo 7!') : Text('Scheduled Appointment for Condo 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c7 ? Text('Removed Condo 7!') : Text('Saved Condo 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/c8.jpg', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Condo 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sc8 ? Text('Removed Appointment for Condo 8!') : Text('Scheduled Appointment for Condo 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('c8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sc8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: c8 ? Text('Removed Condo 8!') : Text('Saved Condo 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveCondo('c8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: c8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Apartments TAB
///-----------------------------------------------------------------------------
class Apartments extends StatefulWidget {
  @override
  _ApartmentsState createState() => _ApartmentsState();
}

class _ApartmentsState extends State<Apartments> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a1.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa1 ? Text('Removed Appointment for Apartment 1!') : Text('Scheduled Appointment for Apartment 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a1 ? Text('Removed Apartment 1!') : Text('Saved Apartment 1!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a1');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a2.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Apartment 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sa2 ? Text('Removed Appointment for Apartment 2!') : Text('Scheduled Appointment for Apartment 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('a2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sa2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: a2 ? Text('Removed Apartment 2!') : Text('Saved Apartment 2!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveApartment('a2');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: a2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a3.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa3 ? Text('Removed Appointment for Apartment 3!') : Text('Scheduled Appointment for Apartment 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a3 ? Text('Removed Apartment 3!') : Text('Saved Apartment 3!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a3');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a4.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Apartment 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sa4 ? Text('Removed Appointment for Apartment 4!') : Text('Scheduled Appointment for Apartment 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('a4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sa4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: a4 ? Text('Removed Apartment 4!') : Text('Saved Apartment 4!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveApartment('a4');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: a4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a5.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa5 ? Text('Removed Appointment for Apartment 5!') : Text('Scheduled Appointment for Apartment 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a5 ? Text('Removed Apartment 5!') : Text('Saved Apartment 5!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a5');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a6.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Apartment 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sa6 ? Text('Removed Appointment for Apartment 6!') : Text('Scheduled Appointment for Apartment 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('a6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sa6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: a6 ? Text('Removed Apartment 6!') : Text('Saved Apartment 6!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveApartment('a6');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: a6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a7.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa7 ? Text('Removed Appointment for Apartment 7!') : Text('Scheduled Appointment for Apartment 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a7 ? Text('Removed Apartment 7!') : Text('Saved Apartment 7!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a7');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ///Image
                ///-------------------------------------------------------------
                SizedBox(
                  child: Image.asset('images/a8.png', fit: BoxFit.fill, width: width, height: height, ),
                  width: width,
                ),
                ///-------------------------------------------------------------

                ///Column of Icons with ToolTips
                ///-------------------------------------------------------------
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Apartment 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: sa8 ? Text('Removed Appointment for Apartment 8!') : Text('Scheduled Appointment for Apartment 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        scheduleAppointment('a8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Schedule Appointment with a RealState Agent',
                          child: sa8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Scaffold.of(context).showSnackBar(SnackBar(
                          content: a8 ? Text('Removed Apartment 8!') : Text('Saved Apartment 8!'),
                          duration: Duration(seconds: 1),
                        ));
                        saveApartment('a8');},
                      child: Container(
                        child: Tooltip(
                          message: 'Save For Later',
                          child: a8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                        ),
                      ),
                    ),
                    ///---------------------------------------------------------
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

///Library Page
///-----------------------------------------------------------------------------
class Library extends StatefulWidget {
  @override
  _LibraryState createState() => _LibraryState();
}

class _LibraryState extends State<Library> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.pink[400],
            title: Text('Explore'),
            bottom: TabBar(
              isScrollable: false,
              tabs: <Widget>[
                Tab(
                  text: 'Scheduled Appointments',
                  icon: Icon(Icons.schedule),
                ),
                Tab(
                  text: 'Saved from Explore',
                  icon: Icon(Icons.save_alt),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[  ScheduledAppointments(), SavedFromExplore() ],
          ),
        )
    );
  }
}

///ScheduledAppointments
///-----------------------------------------------------------------------------
class ScheduledAppointments extends StatefulWidget {
  @override
  _ScheduledAppointmentsState createState() => _ScheduledAppointmentsState();
}

class _ScheduledAppointmentsState extends State<ScheduledAppointments> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          DataTable(
            columns: <DataColumn>[
              DataColumn(label: Text('House', style: TextStyle(fontStyle: FontStyle.italic),),),
              DataColumn(label: Text('Agent', style: TextStyle(fontStyle: FontStyle.italic),),),
              DataColumn(label: Text('Scheduled?', style: TextStyle(fontStyle: FontStyle.italic),),),
            ],
            rows: <DataRow>[
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 1')),
                  DataCell(Text('David')),
                  sh1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 2')),
                  DataCell(Text('Asma')),
                  sh2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 3')),
                  DataCell(Text('Mane')),
                  sh3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 4')),
                  DataCell(Text('Edgar')),
                  sh4 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 5')),
                  DataCell(Text('Mane')),
                  sh5 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 6')),
                  DataCell(Text('David')),
                  sh6 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 7')),
                  DataCell(Text('Edgar')),
                  sh7 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Home 8')),
                  DataCell(Text('Edgar')),
                  sh8 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),

              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 1')),
                  DataCell(Text('Asma')),
                  sc1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 2')),
                  DataCell(Text('Mane')),
                  sc2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 3')),
                  DataCell(Text('David')),
                  sc3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 4')),
                  DataCell(Text('Asma')),
                  sc4 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 5')),
                  DataCell(Text('Edgar')),
                  sc5 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 6')),
                  DataCell(Text('Asma')),
                  sc6 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 7')),
                  DataCell(Text('Edgar')),
                  sc7 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Condo 8')),
                  DataCell(Text('Asma')),
                  sc8 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),

              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 1')),
                  DataCell(Text('Mane')),
                  sa1 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 2')),
                  DataCell(Text('Asma')),
                  sa2 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 3')),
                  DataCell(Text('David')),
                  sa3 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 4')),
                  DataCell(Text('Mane')),
                  sa4 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 5')),
                  DataCell(Text('Mane')),
                  sa5 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 6')),
                  DataCell(Text('Edgar')),
                  sa6 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 7')),
                  DataCell(Text('Mane')),
                  sa7 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('Apartment 8')),
                  DataCell(Text('David')),
                  sa8 ? DataCell(Icon(Icons.check)) : DataCell(Text('')),
                ],
              ),

            ],
          ),
        ],
      ),
    );
  }
}

///SavedFromExplore
///-----------------------------------------------------------------------------
class SavedFromExplore extends StatefulWidget {
  @override
  _SavedFromExploreState createState() => _SavedFromExploreState();
}

class _SavedFromExploreState extends State<SavedFromExplore> {
  final savedSnack = SnackBar(
    content: Text('Saved!'),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ///Home
          ///----------------------------------------------------------------------------------------------------------
          Visibility(
            visible: h1,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h1.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Home 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sh1 ? Text('Removed Appointment for Home 1!') : Text('Scheduled Appointment for Home 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('h1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sh1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: h1 ? Text('Removed Home 1!') : Text('Saved Home 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveHome('h1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: h1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h2,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h2.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh2 ? Text('Removed Appointment for Home 2!') : Text('Scheduled Appointment for Home 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h2 ? Text('Removed Home 2!') : Text('Saved Home 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h3,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h3.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Home 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sh3 ? Text('Removed Appointment for Home 3!') : Text('Scheduled Appointment for Home 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('h3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sh3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: h3 ? Text('Removed Home 3!') : Text('Saved Home 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveHome('h3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: h3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h4,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h4.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh4 ? Text('Removed Appointment for Home 4!') : Text('Scheduled Appointment for Home 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h4 ? Text('Removed Home 4!') : Text('Saved Home 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h5,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h5.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Home 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sh4 ? Text('Removed Appointment for Home 5!') : Text('Scheduled Appointment for Home 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('h5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sh5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: h5 ? Text('Removed Home 5!') : Text('Saved Home 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveHome('h5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: h5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h6,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h6.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh6 ? Text('Removed Appointment for Home 6!') : Text('Scheduled Appointment for Home 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h6 ? Text('Removed Home 6!') : Text('Saved Home 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h7,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h7.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Home 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sh7 ? Text('Removed Appointment for Home 7!') : Text('Scheduled Appointment for Home 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('h7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sh7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: h7 ? Text('Removed Home 7!') : Text('Saved Home 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveHome('h7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: h7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: h8,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/h8.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Home 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sh8 ? Text('Removed Appointment for Home 8!') : Text('Scheduled Appointment for Home 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('h8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sh8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: h8 ? Text('Removed Home 8!') : Text('Saved Home 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveHome('h8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: h8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),

          ///Condos
          ///----------------------------------------------------------------------------------------------------------
          Visibility(
            visible: c1,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c1.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Condo 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sc1 ? Text('Removed Appointment for Condo 1!') : Text('Scheduled Appointment for Condo 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('c1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sc1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: c1 ? Text('Removed Condo 1!') : Text('Saved Condo 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveCondo('c1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: c1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c2,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c2.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc2 ? Text('Removed Appointment for Condo 2!') : Text('Scheduled Appointment for Condo 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c2 ? Text('Removed Condo 2!') : Text('Saved Condo 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c3,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c3.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Condo 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sc3 ? Text('Removed Appointment for Condo 3!') : Text('Scheduled Appointment for Condo 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('c3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sc3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: c3 ? Text('Removed Condo 3!') : Text('Saved Condo 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveCondo('c3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: c3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c4,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c4.jpeg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc4 ? Text('Removed Appointment for Condo 4!') : Text('Scheduled Appointment for Condo 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c4 ? Text('Removed Condo 4!') : Text('Saved Condo 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c5,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c5.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Condo 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sc5 ? Text('Removed Appointment for Condo 5!') : Text('Scheduled Appointment for Condo 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('c5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sc5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: c5 ? Text('Removed Condo 5!') : Text('Saved Condo 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveCondo('c5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: c5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c6,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c6.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc6 ? Text('Removed Appointment for Condo 6!') : Text('Scheduled Appointment for Condo 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c6 ? Text('Removed Condo 6!') : Text('Saved Condo 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c7,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c7.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Condo 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sc7 ? Text('Removed Appointment for Condo 7!') : Text('Scheduled Appointment for Condo 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('c7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sc7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: c7 ? Text('Removed Condo 7!') : Text('Saved Condo 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveCondo('c7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: c7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: c8,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/c8.jpg', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Condo 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sc8 ? Text('Removed Appointment for Condo 8!') : Text('Scheduled Appointment for Condo 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('c8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sc8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: c8 ? Text('Removed Condo 8!') : Text('Saved Condo 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveCondo('c8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: c8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),

          ///Apartments
          ///----------------------------------------------------------------------------------------------------------
          Visibility(
            visible: a1,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a1.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Apartment 1', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sa1 ? Text('Removed Appointment for Apartment 1!') : Text('Scheduled Appointment for Apartment 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('a1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sa1 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: a1 ? Text('Removed Apartment 1!') : Text('Saved Apartment 1!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveApartment('a1');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: a1 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a2,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a2.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 2', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa2 ? Text('Removed Appointment for Apartment 2!') : Text('Scheduled Appointment for Apartment 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa2 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a2 ? Text('Removed Apartment 2!') : Text('Saved Apartment 2!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a2');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a2 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a3,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a3.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Apartment 3', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sa3 ? Text('Removed Appointment for Apartment 3!') : Text('Scheduled Appointment for Apartment 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('a3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sa3 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: a3 ? Text('Removed Apartment 3!') : Text('Saved Apartment 3!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveApartment('a3');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: a3 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a4,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a4.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 4', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa4 ? Text('Removed Appointment for Apartment 4!') : Text('Scheduled Appointment for Apartment 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa4 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a4 ? Text('Removed Apartment 4!') : Text('Saved Apartment 4!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a4');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a4 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a5,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a5.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Apartment 5', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sa5 ? Text('Removed Appointment for Apartment 5!') : Text('Scheduled Appointment for Apartment 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('a5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sa5 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: a5 ? Text('Removed Apartment 5!') : Text('Saved Apartment 5!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveApartment('a5');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: a5 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a6,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a6.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 6', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa6 ? Text('Removed Appointment for Apartment 6!') : Text('Scheduled Appointment for Apartment 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa6 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a6 ? Text('Removed Apartment 6!') : Text('Saved Apartment 6!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a6');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a6 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a7,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a7.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Apartment 7', style: TextStyle(fontSize: 15, color: Colors.pink),),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: sa7 ? Text('Removed Appointment for Apartment 7!') : Text('Scheduled Appointment for Apartment 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            scheduleAppointment('a7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Schedule Appointment with a RealState Agent',
                              child: sa7 ? Icon(Icons.remove) : Icon(Icons.schedule),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: a7 ? Text('Removed Apartment 7!') : Text('Saved Apartment 7!'),
                              duration: Duration(seconds: 1),
                            ));
                            saveApartment('a7');},
                          child: Container(
                            child: Tooltip(
                              message: 'Save For Later',
                              child: a7 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                            ),
                          ),
                        ),
                        ///---------------------------------------------------------
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: a8,
            child: Card(
              color: Colors.grey[200],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ///Image
                  ///-------------------------------------------------------------
                  SizedBox(
                    child: Image.asset('images/a8.png', fit: BoxFit.fill, width: width, height: height, ),
                    width: width,
                  ),
                  ///-------------------------------------------------------------

                  ///Column of Icons with ToolTips
                  ///-------------------------------------------------------------
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Apartment 8', style: TextStyle(fontSize: 15, color: Colors.pink),),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: sa8 ? Text('Removed Appointment for Apartment 8!') : Text('Scheduled Appointment for Apartment 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          scheduleAppointment('a8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Schedule Appointment with a RealState Agent',
                            child: sa8 ? Icon(Icons.remove) : Icon(Icons.schedule),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: a8 ? Text('Removed Apartment 8!') : Text('Saved Apartment 8!'),
                            duration: Duration(seconds: 1),
                          ));
                          saveApartment('a8');},
                        child: Container(
                          child: Tooltip(
                            message: 'Save For Later',
                            child: a8 ? Icon(Icons.delete) : Icon(Icons.save_alt),
                          ),
                        ),
                      ),
                      ///---------------------------------------------------------
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}